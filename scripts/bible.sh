#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   All Glory To God. Jesus is Lord
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

get_verse() {
    local type="${1:-random}"
    local url="https://labs.bible.org/api/?passage=$type&type=json"
    local verse="$(curl -s "$url" | jq -r '.[0]')"
    echo "$verse"   # Return the entire verse JSON object
}

# Function to format and return a Bible verse in the desired format
format_verse() {
    local verse_json="$1"
    echo "$verse_json" | jq -r '"\(.bookname) \(.chapter):\(.verse) - \(.text)"'
}

# Function to get and format a Bible verse
get_formatted_verse() {
    local type="${1:-random}"
    local verse_json="$(get_verse "$type")"
    format_verse "$verse_json"
}

# Example of how to use the script
# source bible.sh
# formatted_verse="$(get_formatted_verse)"
# echo "$formatted_verse"
# John 3:16 For this is the way God loved the world: \
#      He gave his one and only Son, so that everyone who believes in him will \
#      not perish but have eternal life.
